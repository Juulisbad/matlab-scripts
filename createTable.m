clear all;

%Direcotry path of files
files = dir('Xsens_data/*.xlsx');

%Read the two table data
personalData = readtable('Probandeninformation');
orthelligentData = readtable('Orthelligent_data');

%Create Variable for first foot
erstesBein = 'nichts';

%Parameter Names in Array
temp = [14 19];
varNames = ["ID","passiv_sitzend_rechts", "passiv_sitzend_links", "aktiv_sitzend_rechts", "aktiv_sitzend_links", "aktiv_rechts", "aktiv_links", "defizit_rechts", "defizit_links","defizit_rechtslinks","passiv_sitzend_rechts_xsens", "passiv_sitzend_links_xsens", "aktiv_sitzend_rechts_xsens", "aktiv_sitzend_links_xsens", "aktiv_rechts_xsens", "aktiv_links_xsens", "defizit_rechts_xsens", "defizit_links_xsens","defizit_rechtslinks_xsens"];
varTypes = ["string", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double"];
tabelle = table('Size', temp,'VariableTypes', varTypes, 'VariableNames', varNames);


%Loop through every file
for fileNo = 1:14
    
   %Loop through every task
    for task = 1:24
        baseFileName = files(fileNo).name;
        %Define the naming of the files
        if fileNo < 10
            baseFileName = sprintf(['P0', num2str(fileNo), 'V', num2str(task)]);
        else
            baseFileName = sprintf(['P', num2str(fileNo), 'V', num2str(task)]);
        end
        %Define full file name
        fullFileName = fullfile('Xsens_data/', baseFileName);

        %Read table with Xsens Data
        %Read to different table places depending on task
        switch task
            case 1
                psr1 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 2
                psr2 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 3
                psr3 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 13
                psl1 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 14
                psl2 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 15
                psl3 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 4
                asr1 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 5 
                asr2 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 6
                asr3 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 16
                asl1 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 17
                asl2 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 18
                asl3 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 7
                ar1 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 8
                ar2 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 9
                ar3 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 19 
                al1 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 20
                al2 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 21
                al3 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 10
                dr1 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 11
                dr2 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 12
                dr3 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 22
                dl1 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 23
                dl2 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            case 24 
                dl3 = readtable(fullFileName, 'Sheet', 'Joint Angles ZXY');
            otherwise
                error('Wrong Task')
        end

    end
    
    
    %Look if right or left leg was used first
    if strcmp(personalData.erstesBein(fileNo),'rechts')
        erstesBein = 'RightKneeFlexion_Extension';
        zweitesBein = 'LeftKneeFlexion_Extension';
    elseif strcmp(personalData.erstesBein(fileNo),'links')
        erstesBein = 'LeftKneeFlexion_Extension';
        zweitesBein = 'RightKneeFlexion_Extension';
    else 
        error('Ungültiges Bein')
    end
    
    
    
    
    tabelle.ID(fileNo) = fileNo;
    
    %Fill table with Xsens and Orthelligent data
    for i=1:3
        tabelle.passiv_sitzend_rechts(fileNo) = (tabelle.passiv_sitzend_rechts(fileNo) + orthelligentData.passiv_sitzendRechts((fileNo-1)*3+i)/3);
        tabelle.passiv_sitzend_links(fileNo) = (tabelle.passiv_sitzend_links(fileNo) +orthelligentData.passiv_sitzendLinks((fileNo-1)*3+i)/3);
        tabelle.aktiv_sitzend_rechts(fileNo) = (tabelle.aktiv_sitzend_rechts(fileNo) + orthelligentData.aktiv_sitzendRechts((fileNo-1)*3+i)/3);
        tabelle.aktiv_sitzend_links(fileNo) = (tabelle.aktiv_sitzend_links(fileNo) + orthelligentData.aktiv_sitzendLinks((fileNo-1)*3+i)/3);    
        tabelle.aktiv_rechts(fileNo) = (tabelle.aktiv_rechts(fileNo) + orthelligentData.aktiv_stehendRechts((fileNo-1)*3+i)/3);
        tabelle.aktiv_links(fileNo) = (tabelle.aktiv_links(fileNo) + orthelligentData.aktiv_stehendLinks((fileNo-1)*3+i)/3);
        tabelle.defizit_rechts(fileNo) = (tabelle.defizit_rechts(fileNo) + orthelligentData.defizitRechts((fileNo-1)*3+i)/3);
        tabelle.defizit_links(fileNo) = (tabelle.defizit_links(fileNo) + orthelligentData.defizitLinks((fileNo-1)*3+i)/3);
        tabelle.defizit_rechtslinks(fileNo) = (tabelle.defizit_rechtslinks(fileNo) + orthelligentData.defizitRechts_links((fileNo-1)*3+i)/3);

    end
            
       
        
        
        % Create mean values of three measurement repetitions
        temp1 = max(psr1.(genvarname(erstesBein)));
        temp2 = max(psr2.(genvarname(erstesBein)));
        temp3 = max(psr3.(genvarname(erstesBein)));

        tabelle.passiv_sitzend_rechts_xsens(fileNo) = (temp1 + temp2 + temp3) / 3;
        
        
        temp1 = max(psl1.(genvarname(zweitesBein)));
        temp2 = max(psl2.(genvarname(zweitesBein)));
        temp3 = max(psl3.(genvarname(zweitesBein))); 
        tabelle.passiv_sitzend_links_xsens(fileNo) = (temp1 + temp2 + temp3) / 3;
        
        
        temp1 = max(asr1.(genvarname(erstesBein)));
        temp2 = max(asr2.(genvarname(erstesBein)));
        temp3 = max(asr3.(genvarname(erstesBein))); 
        tabelle.aktiv_sitzend_rechts_xsens(fileNo) = (temp1 + temp2 + temp3) / 3;
        
        temp1 = max(asl1.(genvarname(zweitesBein)));
        temp2 = max(asl2.(genvarname(zweitesBein)));
        temp3 = max(asl3.(genvarname(zweitesBein))); 
        tabelle.aktiv_sitzend_links_xsens(fileNo) = (temp1 + temp2 + temp3) / 3;
        
        
        temp1 = max(ar1.(genvarname(erstesBein)));
        temp2 = max(ar2.(genvarname(erstesBein)));
        temp3 = max(ar3.(genvarname(erstesBein))); 
        tabelle.aktiv_rechts_xsens(fileNo) = (temp1 + temp2 + temp3) / 3;
        
        temp1 = max(al1.(genvarname(zweitesBein)));
        temp2 = max(al2.(genvarname(zweitesBein)));
        temp3 = max(al3.(genvarname(zweitesBein))); 
        tabelle.aktiv_links_xsens(fileNo) = (temp1 + temp2 + temp3) / 3;
        
        temp1 = min(dr1.(genvarname(erstesBein)));
        temp2 = min(dr2.(genvarname(erstesBein)));
        temp3 = min(dr3.(genvarname(erstesBein))); 
        tabelle.defizit_rechts_xsens(fileNo) = (temp1 + temp2 + temp3) / 3;
        
        temp1 = min(dl1.(genvarname(zweitesBein)));
        temp2 = min(dl2.(genvarname(zweitesBein)));
        temp3 = min(dl3.(genvarname(zweitesBein))); 

        tabelle.defizit_links_xsens(fileNo) = (temp1 + temp2 + temp3) / 3;
        
        %Create Stretchdeficit from values of left and right leg
        tabelle.defizit_rechtslinks_xsens(fileNo) = abs(tabelle.defizit_rechts_xsens(fileNo)) - abs(tabelle.defizit_links_xsens(fileNo));




end

%Save Table
writetable(tabelle, 'durchschnitt.xlsx')

        
        
        